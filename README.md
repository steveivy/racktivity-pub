# Racktivity Pub

May the gods of [#racket](https://racket-lang.org/) and the social web forgive me.

## What is this?

1. A place for me to learn #racket, and maybe contribute something to the new social web as well.
2. A Racket library useful for building [ActivityPub](https://activitypub.rocks/) applications.
3. My latest ADHD hyperfixation, soon to be abandoned (let's hope not)

## Links

- [ActivityPub Site](https://activitypub.rocks/)
- [W3C Spec Document](https://www.w3.org/TR/activitypub/)